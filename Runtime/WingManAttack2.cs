﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingManAttack2 : MonoBehaviour
{
        public int m_playerId=0;
        private Player m_player;
        public float m_speed=7f;
        public float m_range=3f;
        public float m_horizontalRotation=180;
        public float m_verticalRotation=180;
        public float m_verticalTransition=10;

    public Vector3 m_axis;
    public bool m_primaryFiring;
    public bool m_secondaryFiring;
    public bool m_menu;
    public bool m_pause;
    public bool m_thumbUp;
    public bool m_thumbDown;

    public GameObject m_mainWeapon;
    public GameObject m_secondaryWeapon;
    void Awake()
        {
            m_player = ReInput.players.GetPlayer(m_playerId);
        }

        public void Update()
    {
        m_axis.x = m_player.GetAxis("Horizontal");
        m_axis.y = m_player.GetAxis("Vertical");
        m_axis.z = m_player.GetAxis("Speed");
        m_primaryFiring = m_player.GetButton("MainFire");
        m_secondaryFiring = m_player.GetButton("SecondFire");
        m_thumbUp = m_player.GetButton("Up");
        m_thumbDown = m_player.GetButton("Down");
        m_menu = m_player.GetButton("Menu");
        m_pause = m_player.GetButton("Pause");

        m_mainWeapon.SetActive(m_primaryFiring);
        m_secondaryWeapon.SetActive(m_secondaryFiring);
        float translateVertical=0;
        if(m_thumbUp)
        translateVertical += 1f;
        if (m_thumbDown)
            translateVertical -= 1f;

        transform.Translate(Vector3.up * translateVertical * m_verticalTransition * Time.deltaTime, Space.Self);

        transform.Translate(Vector3.forward * (m_speed + (m_range * m_player.GetAxis("Speed"))) * Time.deltaTime, Space.Self);
            transform.Rotate(
                new Vector3(m_player.GetAxis("Vertical") * m_horizontalRotation * Time.deltaTime, 0
                , m_player.GetAxis("Horizontal") * m_horizontalRotation * Time.deltaTime), Space.Self);

        }
    }

